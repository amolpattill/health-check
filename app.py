from flask import Flask
import time

app = Flask(__name__)
start_time = time.time()

@app.route('/')
def index():
    if time.time() - start_time < 20:
        return "Success", 200
    else:
        return "Server Error", 500


if __name__ == "__main__":
    app.run(host="0.0.0.0", port=5000)
